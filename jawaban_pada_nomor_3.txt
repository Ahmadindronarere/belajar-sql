User

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES ('01', 'John Doe', 'john@doe.com', 'john123');
INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES ('02', 'Jane Doe', 'jane@doe.com', 'jenita123');


categories

INSERT INTO `categories` (`id`, `name`) VALUES ('01', 'gadget');
INSERT INTO `categories` (`id`, `name`) VALUES ('02', 'cloth');
INSERT INTO `categories` (`id`, `name`) VALUES ('03', 'men');
INSERT INTO `categories` (`id`, `name`) VALUES ('04', 'women');
INSERT INTO `categories` (`id`, `name`) VALUES ('05', 'branded');


items

INSERT INTO `items` (`id`, `name`, `description`, `price`, `stock`, `category_id`) VALUES ('01', 'Sumsang b50', 'hape keren dari merek sumsang', '4000000', ' 100', '1');

INSERT INTO `items` (`id`, `name`, `description`, `price`, `stock`, `category_id`) VALUES ('02', 'Uniklooh', 'baju keren dari brand ternama', '500000', ' 50', '2');

INSERT INTO `items` (`id`, `name`, `description`, `price`, `stock`, `category_id`) VALUES ('03', 'IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1');


